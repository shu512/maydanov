#include "header.h"

char lastValue[BUF_SIZE] = "";

// read from file servers ips and ports
void read_server_lists() {
  FILE *file_lists;
  
  file_lists = fopen (FILENAME, "r");
  if (!file_lists) {
      printf("ERROR: file '%s' not found.\n", FILENAME);
      exit(-1);
  }
  fscanf (file_lists, "%d", &count_servers);
  for(int i = 0; i < count_servers; i++) {
    fscanf (file_lists, "%s%d", ips[i], &ports[i]);
  }
  fclose(file_lists);

  for(int i = 0; i < count_servers; i++) {
    servers_addr[i].sin_family = AF_INET;
	  servers_addr[i].sin_port = htons(ports[i]);
	  servers_addr[i].sin_addr.s_addr = inet_addr(ips[i]);
  }
}

// communication with server
int communication(int sd, struct sockaddr_in saddr) {
  int buf_send;
  int buf_read;
  char buf[BUF_SIZE];
  char sendValue[1024];

  // if new server not available
  if (connect(sd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0)
    return -1;
  // if previous server was down
  if (strlen(lastValue) > 0) {
    buf_send = send(sd, lastValue, strlen(lastValue) + 1, 0);
    lastValue[0] = '\0';
    buf_read = recv(sd, buf, BUF_SIZE, 0);
    if (buf_read <= 0) {
      strncpy(lastValue, sendValue, BUF_SIZE);
      return -1;
    }
    printf("%s\n", buf);
  }

  while(1) {
    fgets(sendValue, 80, stdin);
    sendValue[strlen(sendValue)] = '\0'; // cut '\n'
    if (strcmp(sendValue, "exit\n") == 0) {
      break;
    }
    buf_send = send(sd, sendValue, strlen(sendValue) + 1, 0);
    buf_read = recv(sd, buf, BUF_SIZE, 0);
    if (buf_read <= 0) { // if server down
      strncpy(lastValue, sendValue, strlen(sendValue) - 1); // save sendValue for next server
      return -1;
    }
    printf("%s\n", buf);
  }
  return 0;
}

int main() {
  int sd[count_servers + 1];
  int result;

  read_server_lists();

  for(int i = 0; i < count_servers; i++) {
    if ( (sd[i] = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
      printf("ERROR: failed to create socket\n");
      exit(-1);
    }
    result = communication(sd[i], servers_addr[i]);
    if (result == 0)
      break;
  }  
  if (result != 0) {
    printf("Sorry, servers are currently unavailable. Please, try again later.\n");
  }
  return 0;
}