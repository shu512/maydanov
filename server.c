#include "header.h"

char lastBuf[BUF_SIZE] = "First!";

void init_socket_tcp(int sd, int port) {
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);

  if ( bind(sd, (struct sockaddr *)&saddr, sizeof(saddr)) ) {
    printf("ERROR: failed to bind tcp socket.\n");
		exit(-1);
	}
  if ( fcntl(sd, F_SETFL, O_NONBLOCK) == -1 ) {
    printf("ERROR: failed to set non-blocking tcp socket.\n");
    exit(-1);
  }

  printf("port = %d\n", ntohs(saddr.sin_port));
}

void init_socket_udp(int sd_udp, int port) {
  struct sockaddr_in saddr_udp;
  saddr_udp.sin_family = AF_INET;
	saddr_udp.sin_port = htons(port);
	saddr_udp.sin_addr.s_addr = htonl(INADDR_ANY);

  if ( bind(sd_udp, (struct sockaddr *)&saddr_udp, sizeof(saddr_udp)) < 0 ){
		printf("ERROR: failed to bind udp socket.\n");
		exit(-1);
	}
  if ( fcntl( sd_udp, F_SETFL, O_NONBLOCK) == -1 ) {
    printf("ERROR: failed to set non-blocking udp socket.\n");
    exit(-1);
  }
}

// read from file servers ips and ports
void read_server_lists() {
  FILE *file_lists;
  
  file_lists = fopen (FILENAME, "r");
  if (!file_lists) {
      printf("ERROR: file '%s' not found.\nTerminated.\n", FILENAME);
      exit(-1);
  }
  fscanf (file_lists, "%d", &count_servers);
  for(int i = 0; i < count_servers; i++) {
    fscanf (file_lists, "%s%d", ips[i], &ports[i]);
  }
  fclose(file_lists);

  for(int i = 0; i < count_servers; i++) {
    servers_addr[i].sin_family = AF_INET;
	  servers_addr[i].sin_port = htons(ports[i]);
	  servers_addr[i].sin_addr.s_addr = inet_addr(ips[i]);
  }
}

// check tcp socket
int is_new_client(int sd) {
  fd_set readfds;
  struct timeval tv;

  FD_ZERO(&readfds);
  FD_SET(sd, &readfds);
  tv.tv_sec = 1;
  tv.tv_usec = 0;

  select(sd + 1, &readfds, NULL, NULL, &tv);
  if (FD_ISSET(sd, &readfds)) {
    return 1;
  } else {
    return 0;
  }
}

// send lastBuf all servers
void broadcast(char buf[BUF_SIZE]) {
  int udp_send = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  for(int i = 0; i < count_servers; i++) {
    sendto(udp_send, buf, strlen(buf) + 1, 0,
        (struct sockaddr *)&servers_addr[i], sizeof(servers_addr[i]));
  }
}

// communication with client
void communication(int client) {
  int buf_read = 1;
  char buf[BUF_SIZE];
  char tmp[BUF_SIZE];

  while(true) {
    buf_read = recv(client, buf, BUF_SIZE, 0);
    if (buf_read <= 0)
      break;
    broadcast(buf);

    snprintf(tmp, sizeof(buf), "%s", buf);
    snprintf(buf, sizeof(buf), "%s %s", lastBuf, tmp);
    snprintf(lastBuf, sizeof(lastBuf), "%s", tmp);
    if (lastBuf[strlen(lastBuf) - 1] == '\n') {
      lastBuf[strlen(lastBuf) - 1] = '\0';
    }
    send(client, buf, sizeof(buf), 0);
  }
}

int main(int argc, char *argv[]) {  
  int buf_read = 1;
  char buf[BUF_SIZE];
  int port = DEFAULT_PORT;

  if ( argc > 1 )
		port = atoi(argv[1]);

  read_server_lists();

  int sd = socket(AF_INET, SOCK_STREAM, 0);
  int sd_udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  init_socket_tcp(sd, port);
  init_socket_udp(sd_udp, port);
  listen(sd, 0);
  listen(sd_udp, 0);

  // recive tcp(from client) and udp(from other servers)
  while(1) {
    if (is_new_client(sd)) {
      break;
    }
    buf_read = recvfrom(sd_udp, buf, BUF_SIZE, 0,
        (struct sockaddr *)0, (socklen_t *)0);
    if (buf_read > 0) {
      strncpy(lastBuf, buf, BUF_SIZE);
      if (lastBuf[strlen(lastBuf) - 1] == '\n') {
        lastBuf[strlen(lastBuf) - 1] = '\0';
      }
    }
  }

  int client = accept(sd, (struct sockaddr *)0, 0);
  communication(client);
  return 0;
}