#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>
#include <pthread.h>
#include <curses.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>

#define DEFAULT_ADDR	      "127.0.0.1"
#define DEFAULT_PORT        8888
#define BUF_SIZE            1024
#define MAX_COUNT_SERVERS   10
#define FILENAME            "list servers"

char ips[MAX_COUNT_SERVERS][30];
int ports[MAX_COUNT_SERVERS];
int count_servers;
struct sockaddr_in servers_addr[MAX_COUNT_SERVERS];

#endif